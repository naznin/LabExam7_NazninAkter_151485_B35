<?php
namespace App\Birthdate;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class Birthdate extends DB{
    public $id="";
    public $person_name="";
    public $birthdate="";

    public function __construct(){
    parent:: __construct();
    if(!isset( $_SESSION)) session_start();
}

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('person_name',$postVariableData)){
            $this->person_name = $postVariableData['person_name'];
        }

        if(array_key_exists('birthdate',$postVariableData)){
            $this->birthdate = $postVariableData['birthdate'];
        }
    }



    public function store(){

    $arrData = array( $this->person_name, $this->birthdate);

    $sql = "Insert INTO birthday(person_name, birthdate) VALUES (?,?)";
    $STH = $this->DBH->prepare($sql);

    $result = $STH->execute($arrData);

     if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
     else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');


    }// end of store method




}

?>

