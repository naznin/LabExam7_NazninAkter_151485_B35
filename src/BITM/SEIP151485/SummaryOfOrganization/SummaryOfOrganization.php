<?php
namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Utility\Utility;
use App\Model\Database as DB;


class SummaryOfOrganization extends DB{
    public $id="";
    public $organization_name="";
    public $summary="";

    public function __construct(){
    parent:: __construct();
    if(!isset( $_SESSION)) session_start();
}

    public function setData($postVariableData=NULL){

        if(array_key_exists('id',$postVariableData)){
            $this->id = $postVariableData['id'];
        }

        if(array_key_exists('organization_name',$postVariableData)){
            $this->organization_name = $postVariableData['organization_name'];
        }

        if(array_key_exists('summary',$postVariableData)){
            $this->summary = $postVariableData['summary'];
        }
    }



    public function store(){

    $arrData = array( $this->organization_name, $this->summary);

    $sql = "Insert INTO summary_of_organization(organization_name, summary) VALUES (?,?)";
    $STH = $this->DBH->prepare($sql);

    $result = $STH->execute($arrData);

     if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
     else
            Message::message("Failed! Data Has Not Been Inserted Successfully :(");


        Utility::redirect('create.php');


    }// end of store method







    public function index(){
        echo "I am inside index method of <b>BookTitle</b> Class";
    }




}

?>

